from setuptools import setup

setup(
    name='projet_web_app',
    version='',
    packages=['crazymix', 'crazymix.migrations', 'projet_web_app'],
    url='',
    license='',
    author='Elwiz',
    author_email='elwizmeziani85@gmail.com',
    install_requires=open('requirements.txt').read().split('\n'),
    description=''
)

